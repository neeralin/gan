# GAN HW2
Most code from https://fairyonice.github.io/My-first-GAN-using-CelebA-data.html

## Data
* Get data by https://github.com/NCTU-VRDL/CS_IOC5008/blob/master/HW2/homework2.ipynb
* Data dir `./data/img_align_celeba/`

## Changes
* Discriminator's ReLU to LeakyReLU
* Generator's sigmoid to tanh
* Shuffle the input
* Soft labeling

## Failed changes (no face in result)
* Can't apply to 64x64, although the original code has the `if` in generator, it's useless.
* Change the model structure of G or D
* Scale to [-1, 1]
